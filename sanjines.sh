#!/usr/bin/env bash

#Lista arquivos de vídeo em uma pasta


#Função para listar filmes na pasta Vídeos dentro de ~

catalogo_filmes()
{
    
    formato="(mkv|mp4)" # A ideia aqui é adicionar uma lista de formatos válidos de vídeo
    lista=$(ls ~/Vídeos | grep -E '*.\.(mkv|mp4|avi)$')
    echo "

$lista"

}

catalogo_filmes
