#!/usr/bin/env bash

filme=$(sed -E "s| |\+|g" <<< "$1")                                                                   # Passa $1 para o sed e troca espaço por + e coloca dentro da variável $filme
omdb=$(sed -E "s|\=.*\&|=$filme\&|g" <<< "https://www.omdbapi.com/?t=titulo\&apikey=a18669d5")        # Troca titulo por $filme e coloca dentro da variável $omdb   
metadadosfilmes=$(curl -s $omdb)                                                                      # Coloca os metadados do $filme em $metadadosfilmes

poster=$(grep --color -Eo 'https.+\.jpg' <<< $metadadosfilmes)                                        # Coloca o campo do pôster do filme dentro de $poster

dadosordenados=$(sed -E 's|\,+|\n|g ;                                                                 # Tudo que for seguido de vírgula, acrescenta nova linha
                         s|"||g ;                                                                     # Apaga aspas
                         s|\{||g ;                                                                    # Apaga {
					     s|\}||g ;                                                                    # Apaga }
					     s|\n([0-9]+)|,\1|g ;                                                         # Substitui \n por vírgula seguida de (numeros)
					     s|:([[:alnum:]])|: \1|g ;                                                    # Coloca um espaço em tudo que for seguido de :
					     s|\n ||g;                                                                    # Remove \n
					     s|([a-z])([A-Z])|\1; \2|g ;                                                  # Coloca ;+espaço entre ([a-z])([A-Z])
					     s|imdb; |IMDB|g ; 
					     s|Box; |Box|g  ;
					     s|Poster.+\.jpg||g' <<< $metadadosfilmes)   

sed -E '/^$/d' <<< "$dadosordenados"


  


